from django.db import models

# Create your models here.

class AppAuth(models.Model):
    app_name = models.CharField('名称', max_length=100)
    app_id = models.CharField('APP_ID', max_length=500)
    app_secret = models.CharField('APP_SECRET', max_length=500)
    app_token = models.CharField('APP_TOKEN', max_length=500)

class ChatText(models.Model):
    author = models.CharField('主体', max_length=20)
    contents = models.TextField('内容')