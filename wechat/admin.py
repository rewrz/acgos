from django.contrib import admin

# Register your models here.
from .models import AppAuth, ChatText

class AppAuthAdmin(admin.ModelAdmin):
    list_display = ['app_name', 'app_id', 'app_secret', 'app_token']

class ChatTextAdmin(admin.ModelAdmin):
    list_display = ['author', 'contents']

admin.site.register(AppAuth, AppAuthAdmin)
admin.site.register(ChatText, ChatTextAdmin)