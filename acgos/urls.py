from django.conf.urls import include, url
from django.urls import path

from werobot.contrib.django import make_view
from .wechatRobot import robot

from django.contrib import admin
admin.autodiscover()

import wechat.views

# Examples:
# url(r'^$', 'acgos.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),

urlpatterns = [
    url(r'^$', wechat.views.index, name='index'),
    url(r'^db', wechat.views.db, name='db'),
    # 微信公众号响应页面
    url(r'^wechatRobot/', make_view(robot)),
    path('admin/', admin.site.urls),
]
