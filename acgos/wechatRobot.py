from werobot import WeRoBot
from wechat.models import AppAuth, ChatText
import re
from chatterbot import ChatBot
import addon.weather as weather


# 创建用于回复消息的机器人
chatbot = ChatBot(
    'Hooin Kyoma',
    #read_only=True,# 只读模式
    #trainer='chatterbot.trainers.ListTrainer',# 训练使用列表的模式
    trainer='chatterbot.trainers.ChatterBotCorpusTrainer', # 训练使用文件的模式
    storage_adapter='chatterbot.storage.SQLStorageAdapter',
    # 允许接受许多不同的输入类型
    input_adapter="chatterbot.input.VariableInputTypeAdapter",
    # 允许终端交流
    #input_adapter='chatterbot.input.TerminalAdapter',
    #output_adapter='chatterbot.output.TerminalAdapter',
    logic_adapters=[
        'chatterbot.logic.MathematicalEvaluation',# 数学计算
        #'chatterbot.logic.TimeLogicAdapter',# 时间逻辑
        #'chatterbot.logic.BestMatch',# 优先选用评分值最高解
        {
            'import_path': 'chatterbot.logic.SpecificResponseAdapter',
            'input_text': '傻逼',
            'output_text': '笨蛋！冷静一下！'
        },
        {
            'import_path': 'chatterbot.logic.LowConfidenceAdapter',
            'threshold': 0.50,
            'default_response': '喂？是我。什么？！机关已经开始行动了？！'
        },
        {
            'import_path': 'chatterbot.logic.BestMatch',
            'statement_comparison_function': 'chatterbot.comparisons.levenshtein_distance',
            'response_selection_method': 'chatterbot.response_selection.get_first_response'
        },
    ],
    database='./aidatabase.sqlite3'
)


# 训练使用文件的模式
chatbot.train(
    "chatterbot.corpus.chinese")# 使用该库的中文语料库
# 可以使用指定的语料文件快速训练
#chatterbot.train(
#    "./data/greetings_corpus/custom.corpus.json",
#    "./data/my_corpus/"
#)

app_auth = AppAuth.objects.all()

for auths in app_auth:
    if auths.app_name == '微信机器人':
        robot = WeRoBot(enable_session = False,
                        token = auths.app_token,
                        APP_ID = auths.app_id,
                        APP_SECRET = auths.app_secret)

def is_number(ustr):
    # 判断一个unicode是否是数字
    if ustr >= u'\u0030' and ustr<=u'\u0039':
        return True
    else:
        return False

def is_sign(ustr):
    # 判断一个unicode是否是计算符号（Unicode编码表）
    # 加 减 乘 除  取余 指数 左小括号 右小括号 等于号
    sign = [u'\u002b', u'\u002d', u'\u002a', u'\u002f', u'\u0025', u'\u005e', u'\u0028', u'\u0029', u'\u003d']
    if ustr in sign:
        return True
    else:
        return False


# 实现数字与计算符号之间自动添加一个空格隔开
def numberCalculateChange(string):
    newstring = ""
    stringlist = list(string)
    for i in range(0,len(stringlist)):
        if i<len(stringlist)-1:
            if (is_number(stringlist[i]) and is_sign(stringlist[i+1])):
                 stringlist[i] = stringlist[i]+" "
            elif (is_sign(stringlist[i]) and is_number(stringlist[i+1]) ):
                stringlist[i] = stringlist[i]+" "
        newstring = newstring+stringlist[i]
    return newstring

def airobot(msg):
    '''人工智障回复'''
    if re.compile(".*?今天天气适宜.*?").match(msg) or re.compile(".*?今天天气适合.*?").match(msg):
        city = re.split('今天天气', msg)[0]
        return weather.weather_zhishu(city)
    elif re.compile(".*?天气.*?").match(msg):
        city = re.split('天气', msg)[0]
        return weather.five_day_weather(city)
    else:
        # 机器人自动回复内容
        reply = chatbot.get_response(msg).text
        return reply

# @robot.text 修饰的 Handler 只处理文本消息
@robot.text
def echo(message):
    # 提取消息
    msg = message.content.strip()
    # 解析消息
    return airobot(numberCalculateChange(msg))

# @robot.image 修饰的 Handler 只处理图片消息
@robot.image
def img(message):
    return message.img