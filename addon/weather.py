import requests
# xml解析器，简称ET，性能和便捷综合考虑优先推荐使用
import xml.etree.ElementTree as ET

baseUrl = "http://www.sojson.com/open/api/weather/xml.shtml?city="


def apiUrl(city):
    apiUrl = baseUrl + city
    return apiUrl


def request(url):
    r = requests.get(url)
    return r


def weather_zhishu(city):
    tree = ET.fromstring(
        request(
            apiUrl(city)
        ).text
    )
    zhishus = ''
    zhishu_name = []
    zhishu_value = []
    zhishu_detail = []
    zhishu = []
    for elem in tree.findall(".//zhishu/name"):
        zhishu_name.append(elem.text)
    for elem in tree.findall(".//zhishu/value"):
        zhishu_value.append(elem.text)
    for elem in tree.findall(".//zhishu/detail"):
        zhishu_detail.append(elem.text)

    for i in range(0, len(zhishu_name)):
        zhishu.append("★" + zhishu_name[i] + ":" + zhishu_value[i] + "\n" \
                      + zhishu_detail[i])
    zhishus = '\n'.join(zhishu)
    return zhishus


def five_day_weather(city):
    tree = ET.fromstring(
        request(
            apiUrl(city)
        ).text
    )
    city_name, wendu, fengli, shidu, fengxiang = '暂无数据', '暂无数据', '暂无数据', '暂无数据', '暂无数据'
    aqi, pm25, quality, suggest = '暂无数据', '暂无数据', '暂无数据', '暂无数据'
    for elem in tree.iter(tag="city"):
        city_name = elem.text
    for elem in tree.iter(tag="wendu"):
        wendu = elem.text
    for elem in tree.iter(tag="fengli"):
        fengli = elem.text
    for elem in tree.iter(tag="fengli"):
        fengli = elem.text
    for elem in tree.iter(tag="shidu"):
        shidu = elem.text
    for elem in tree.iter(tag="fengxiang"):
        fengxiang = elem.text

    weather_date = []
    weather_high = []
    weather_low = []
    weather_day_type = []
    #weather_day_fengxiang = []
    #weather_day_fengli = []
    weather_night_type = []
    #weather_night_fengxiang = []
    #weather_night_fengli = []
    weather = []
    weathers = ''
    for elem in tree.findall("./forecast/weather/date"):
        weather_date.append(elem.text)
    for elem in tree.findall("./forecast/weather/high"):
        weather_high.append(elem.text)
    for elem in tree.findall("./forecast/weather/low"):
        weather_low.append(elem.text)
    for elem in tree.findall("./forecast/weather/day/type"):
        weather_day_type.append(elem.text)
    for elem in tree.findall("./forecast/weather/night/type"):
        weather_night_type.append(elem.text)

    for i in range(0, len(weather_date)):
        weather.append("★" + weather_date[i] + "：" + weather_high[i] + "，" \
                       + weather_low[i] + "\n白天：" + weather_day_type[i] + "；晚间：" + weather_night_type[i])
    weathers = '\n'.join(weather)

    for elem in tree.findall("./environment/aqi"):
        aqi = elem.text
    for elem in tree.findall("./environment/pm25"):
        pm25 = elem.text
    for elem in tree.findall("./environment/quality"):
        quality = elem.text
    for elem in tree.findall("./environment/suggest"):
        suggest = elem.text

    now_weather = (
            "%s当前温度%s℃；风力%s；湿度：%s；风向：%s。\
            \n空气质量指数：%s；PM2.5：%s；评价：%s；建议：%s。\n\n"
            %
            (city_name, wendu, fengli, shidu, fengxiang,
             aqi, pm25, quality, suggest))
    return now_weather + weathers