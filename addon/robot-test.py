#from werobot import WeRoBot
import re
from chatterbot import ChatBot


# 创建用于回复消息的机器人
chatbot = ChatBot(
    'Hooin Kyoma',
    #read_only=True,# 只读模式
    #trainer='chatterbot.trainers.ListTrainer',# 训练使用列表的模式
    trainer='chatterbot.trainers.ChatterBotCorpusTrainer', # 训练使用文件的模式
    storage_adapter='chatterbot.storage.SQLStorageAdapter',
    # 允许接受许多不同的输入类型
    input_adapter="chatterbot.input.VariableInputTypeAdapter",
    # 允许终端交流
    #input_adapter='chatterbot.input.TerminalAdapter',
    #output_adapter='chatterbot.output.TerminalAdapter',
    logic_adapters=[
        'chatterbot.logic.MathematicalEvaluation',# 数学计算
        #'chatterbot.logic.TimeLogicAdapter',# 时间逻辑
        #'chatterbot.logic.BestMatch',# 优先选用评分值最高解
        {
            'import_path': 'chatterbot.logic.SpecificResponseAdapter',
            'input_text': '傻逼',
            'output_text': '笨蛋！冷静一下！'
        },
        {
           'import_path': 'chatterbot.logic.LowConfidenceAdapter',
           'threshold': 0.55,
           'default_response': '喂？是我。什么？！机关已经开始行动了？！'
        },
        {
            'import_path': 'chatterbot.logic.BestMatch',
            'statement_comparison_function': 'chatterbot.comparisons.levenshtein_distance',
            'response_selection_method': 'chatterbot.response_selection.get_first_response'
        },
    ],
    database='./aidata.sqlite3'
)


# 训练使用文件的模式
#chatbot.train(
    #"chatterbot.corpus.chinese",
    #"./ai-data/"
#)
# 使用该库的中文语料库

# 可以使用指定的语料文件快速训练
#chatterbot.train(
#    "./data/greetings_corpus/custom.corpus.json",
#    "./data/my_corpus/"
#)

def is_chinese(ustr):
    # 判断一个unicode是否是汉字
    if ustr>= u'\u4e00' and ustr<=u'\u9fa5':
        return True
    else:
        return False

def is_number(ustr):
    # 判断一个unicode是否是数字
    if ustr >= u'\u0030' and ustr<=u'\u0039':
        return True
    else:
        return False

def is_english(ustr):
    # 判断一个unicode是否是英文字母
    if (ustr >= u'\u0041' and ustr<=u'\u005a') or (ustr >= u'\u0061' and ustr<=u'\u007a'):
        return True
    else:
        return False

def is_erightbracket(ustr):
    # 判断一个unicode是否是英文右括号
    if ustr==u')':
        return True
    else:
        return False

def is_crightbracket(ustr):
    # 判断一个unicode是否是中文右括号
    if ustr==u'\uff09':
        return True
    else:
        return False

def is_sign(ustr):
    # 判断一个unicode是否是计算符号（Unicode编码表）
    # 加 减 乘 除  取余 指数 左小括号 右小括号 等于号
    sign = [u'\u002b', u'\u002d', u'\u002a', u'\u002f', u'\u0025', u'\u005e', u'\u0028', u'\u0029', u'\u003d'] 
    if ustr in sign:
        return True
    else:
        return False

# 实现中英文、数字、计算符号之间自动添加一个空格隔开        
def checkText(teststring):
    newstring = ""
    testlist = list(teststring)
    for i in range(0,len(testlist)):
        if i<len(testlist)-1:
            if (is_number(testlist[i]) and is_sign(testlist[i+1])):
                 testlist[i] = testlist[i]+" "
            elif (is_sign(testlist[i]) and is_number(testlist[i+1]) ):
                testlist[i] = testlist[i]+" "
            elif (is_chinese(testlist[i]) and is_number(testlist[i+1])) or (is_chinese(testlist[i]) and is_english(testlist[i+1])):
                testlist[i] = testlist[i]+" "
            elif (is_number(teststring[i]) and is_chinese(teststring[i+1])):
                testlist[i] = testlist[i]+" "
            elif (is_english(teststring[i]) and is_chinese(teststring[i+1])):
                testlist[i] = testlist[i]+" "
            elif (is_erightbracket(teststring[i]) and is_chinese(teststring[i+1])):
                testlist[i] = testlist[i]+" "
            elif (is_crightbracket(teststring[i]) and is_number(teststring[i+1])) or (is_crightbracket(teststring[i]) and is_english(teststring[i+1])):
                testlist[i] = testlist[i]+" "

        newstring = newstring+testlist[i]

    return newstring

# 实现数字与计算符号之间自动添加一个空格隔开
def numberCalculateChange(string):
    newstring = "" 
    stringlist = list(string)
    for i in range(0,len(stringlist)):
        if i<len(stringlist)-1:
            if (is_number(stringlist[i]) and is_sign(stringlist[i+1])):
                 stringlist[i] = stringlist[i]+" "
            elif (is_sign(stringlist[i]) and is_number(stringlist[i+1]) ):
                stringlist[i] = stringlist[i]+" "
        newstring = newstring+stringlist[i]
    return newstring
    
def airobot(msg):
    '''人工智障回复'''
    # 机器人自动回复内容
    reply = chatbot.get_response(msg).text
    return reply


while True:
    try:
        content = input("你：")
        print("凤凰院凶真：", airobot(numberCalculateChange(content)))
    except(KeyboardInterrupt, EOFError, SystemExit):
        break